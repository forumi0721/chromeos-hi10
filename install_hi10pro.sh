#!/bin/bash

if [ -e /lib/firmware/silead/mssl1680.fw -a -e /lib/modules/$(uname -r)/silead_hi10pro.ko ]; then
	echo "Already install firmware and modules"
	echo "Do you want to continue? (Y/N)"
	read answer
	if [ "${answer}" != "Y" -a "${answer}" != "y" ]; then
		exit
	fi
	echo
fi

firmware_url="https://bitbucket.org/forumi0721/chromeos-hi10/raw/master/silead/gsl1680-chuwi-hi10pro.fw"
module_url="https://bitbucket.org/forumi0721/chromeos-hi10/raw/master/silead/silead_hi10pro-$(uname -r).ko"

temp_dir=$(mktemp -d)

echo "==> Firmware"

echo " -> Downloading"
curl -L "${firmware_url}" -o ${temp_dir}/gsl1680-chuwi-hi10pro.fw

echo " -> Installing"
cp ${temp_dir}/gsl1680-chuwi-hi10pro.fw /lib/firmware/silead/mssl1680.fw
chmod 644 /lib/firmware/silead/mssl1680.fw

echo "==> Done."
echo



echo "==> Module"

echo " -> Downloading"
curl -L "${module_url}" -o ${temp_dir}/silead_hi10pro.ko
if [ -z "$(cat ${temp_dir}/silead_hi10pro.ko | grep '<!DOCTYPE html>')" ]; then
	echo " -> Installing"
	cp ${temp_dir}/silead_hi10pro.ko /lib/modules/$(uname -r)/silead_hi10pro.ko
	chmod 644 /lib/modules/$(uname -r)/silead_hi10pro.ko
	depmod -a
else
	echo
	echo "Cannot found prebuilt module"
	if [ ! -e /usr/local/bin/start-toolchain ]; then
		echo "Do you want to install toolchain? (Y/N)"
		read answer
		echo ${answer}
		if [ "${answer}" != "Y" -a "${answer}" != "y" ]; then
			exit
		fi
		echo

		echo " -> Downloading Toolchain"
		toolchain_url="https://github.com$(curl -s -L https://github.com/sebanc/brunch-toolchain/releases/ | grep "<a href=\".*/brunch_toolchain_.*.tar.gz\"" | head -1 | sed -e 's/^.*<a href="\([^"]*\)".*$/\1/g')"
		curl -L "${toolchain_url}" -o ${temp_dir}/brunch_toolchain.tar.gz
		
		echo " -> Installing Toolchain"
		rm -rf /usr/local/*
		tar zxf ${temp_dir}/brunch_toolchain.tar.gz -C /usr/local
	fi

	echo " -> Downloading Module Source"
	curl -L https://bitbucket.org/forumi0721/chromeos-hi10/get/master.zip -o ${temp_dir}/chromeos-hi10.zip
	pushd ${temp_dir} &> /dev/null
	unzip chromeos-hi10.zip
	popd &> /dev/null
	
	echo " -> Building Module"
	pushd ${temp_dir}/forumi0721-chromeos-hi10-* &> /dev/null
	cd silead/silead_hi10pro
	make
	popd &> /dev/null
	
	echo " -> Installing Module"
	pushd ${temp_dir}/forumi0721-chromeos-hi10-* &> /dev/null
	cd silead/silead_hi10pro
	cp silead_hi10pro.ko /lib/modules/$(uname -r)/
	depmod -a
	popd &> /dev/null
fi
echo "==> Done."
echo

echo "==> Cleanup"
rm -rf ${temp_dir}
echo "==> Done."
echo

echo "==> README"
echo "    You have to add kernel parameter to block previous module"
echo "    \`module_blacklist=silead,gslx680_ts_acpi\`"
echo "==> Done."
echo

