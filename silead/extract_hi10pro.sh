#!/bin/sh

variant=hi10pro
firmware=gsl1680-chuwi-${variant,,}.fw

gsl-firmware/tools/scanwindrv SileadTouch.sys.${variant}
mv firmware_00.fw ${firmware}
gsl-firmware/tools/fwtool -c ${firmware} -m 1680 -w 1260 -h 1925 -t 10 -f track,swap silead_ts-${variant}.fw

