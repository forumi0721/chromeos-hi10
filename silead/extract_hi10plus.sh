#!/bin/sh

variant=hi10plus
firmware=gsl1680-chuwi-${variant,,}.fw

gsl-firmware/tools/scanwindrv SileadTouch.sys.${variant}
mv firmware_00.fw ${firmware}
gsl-firmware/tools/fwtool -c ${firmware} -m 1680 -w 1925 -h 1260 -t 10 -f track silead_ts-${variant}.fw

