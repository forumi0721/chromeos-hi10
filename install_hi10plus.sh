#!/bin/sh

if [ -e /lib/firmware/silead/gsl1680-chuwi-hi10plus.fw ]; then
	echo "Already install firmware"
	echo "Do you want to continue? (Y/N)"
	read answer
	if [ "${answer}" != "Y" -a "${answer}" != "y" ]; then
		exit
	fi
	echo
fi

silead_firmware_url="https://bitbucket.org/forumi0721/chromeos-hi10/raw/master/silead/gsl1680-chuwi-hi10plus.fw"
#sst_firmware_url="https://bitbucket.org/forumi0721/chromeos-hi10/raw/master/firmware/fw_sst_22a8.bin"
shisp_firmware_url="https://bitbucket.org/forumi0721/chromeos-hi10/raw/master/firmware/shisp_2401a0_v21.bin"

temp_dir=$(mktemp -d)

echo "==> silead Firmware"

echo " -> Downloading"
curl -L "${silead_firmware_url}" -o ${temp_dir}/gsl1680-chuwi-hi10plus.fw

echo " -> Installing"
mkdir -p /lib/firmware/silead
cp ${temp_dir}/gsl1680-chuwi-hi10plus.fw /lib/firmware/silead/gsl1680-chuwi-hi10plus.fw
chmod 644 /lib/firmware/silead/gsl1680-chuwi-hi10plus.fw

echo " -> Reload Module"
rmmod silead
modprobe silead

echo "==> Done."
echo

#echo "==> sst Firmware"
#
#echo " -> Downloading"
#curl -L "${sst_firmware_url}" -o ${temp_dir}/fw_sst_22a8.bin
#
#echo " -> Installing"
#mkdir -p /lib/firmware/intel
#cp ${temp_dir}/fw_sst_22a8.bin /lib/firmware/intel/fw_sst_22a8.bin
#chmod 644 /lib/firmware/intel/fw_sst_22a8.bin
#
#echo "==> Done."
#echo

echo "==> shisp Firmware"

echo " -> Downloading"
curl -L "${shisp_firmware_url}" -o ${temp_dir}/shisp_2401a0_v21.bin

echo " -> Installing"
mkdir -p /lib/firmware
cp ${temp_dir}/shisp_2401a0_v21.bin /lib/firmware/shisp_2401a0_v21.bin
chmod 644 /lib/firmware/shisp_2401a0_v21.bin

echo "==> Done."
echo

echo "==> Cleanup"
rm -rf "${temp_dir}"
echo "==> Done."
echo

